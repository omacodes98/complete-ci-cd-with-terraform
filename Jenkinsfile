def gv

pipeline {

    agent any
    tools {
        maven 'Maven'
    }
    
    stages {
        stage("increment version"){
            steps {
                script{
                    echo "incrementing app version"
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage("build app") {
            steps {
                script {
                     echo "building the application....."
                     sh 'mvn clean package'
                }
            }
        }
        stage("build image") {
            steps {
                script {
                   echo "building the docker image..."
                   withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                      sh "docker build -t omacodes98/demo-app:jma-${IMAGE_NAME} ."
                      sh "echo $PASS | docker login -u $USER --password-stdin"
                      sh "docker push omacodes98/demo-app:jma-${IMAGE_NAME}"
                    }
                }
            }
        }
        stage("provision server") {
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key_id')
                TF_VAR_env_prefix = 'test'
            }
            steps {
                script {
                    dir('terraform') {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUBLIC_IP = sh(
                            script: "terraform output ec2_public_ip",
                            returnStdout: true
                        ).trim()

                    }
                }
            }
        }
        stage("deploy") {
            environment{
                DOCKER_CREDS = credentials('docker-hub-repo')
            }
            steps {
                script {
                    echo "waiting for ec2 server to Initialize"
                    sleep(time: 90, unit: "SECONDS")

                    echo 'deploying docker image to EC2'
                    echo "${EC2_PUBLIC_IP}"

                    def shellCmd = "bash ./server-cmds.sh omacodes98/demo-app:jma-${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"
                    def ec2Instance = "ec2-user@${EC2_PUBLIC_IP}"

                    sshagent(['server-ssh-key']) {
                       sh "scp -o StrictHostkeyChecking=no server-cmds.sh ec2-user@${EC2_PUBLIC_IP}:/home/ec2-user"
                       sh "scp -o StrictHostkeyChecking=no docker-compose.yaml ec2-user@${EC2_PUBLIC_IP}:/home/ec2-user"
                       sh "ssh -o StrictHostkeyChecking=no ec2-user@${EC2_PUBLIC_IP} ${shellCmd}"
                    }
                }
            }
        }
        stage('commit version update') {
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
                        sh 'git config --global user.email "jenkins@example.com"' 
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/omacodes98/cd-with-jenkins-and-aws.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:complete-ci-cd-with-terraform'
                    }

                }
            }
        }

    }   
}
