# Complete CI/CD with Terraform 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Integrate provisioning stage into complete CI/CD Pipeline to automate provisioning server instead of deploying to an existing server 

* Create SSH Key Pair 

* Install Terrform inside Jenkins container 

* Add Terraform configuration to application's git repository 

* Adjust Jenkinsfile to add "provision" step to the CI/CD pipeline that provisons EC2 instance 

* The Complete CI/CD project has the following configuration:

a. CI step: Build artifact for Java Maven application

b. CI step: Build and push Docker image to Docker Hub

c. CD step: Automatically provision EC2 instance using TF

d. CD step: Deploy new application version on the provisioned EC2 instance with Docker Compose

## Technologies Used 

* Terraform 

* Jenkins 

* Docker 

* AWS 

* Git 

* Java 

* Maven 

* Linux 

* Docker Hub 

* DigitalOcean 

## Steps 

Step 1: Create key pair on AWS for Jenkins credentials to allow Jenkins ssh into ec2 instance that that the application is going to be deployed on 

[AWS key pair creation](/images/01_create_key_pair_on_AWS_for_jenkins_credentials_to_allow_Jenkins_ssh_into_ec2_instance_that_the_application_is_going_to_be_deployed_on.png)

Step 2: Create Jenkins credential for ssh in multibranch pipeline job scope 

[Jenkins ssh credentials creation](/images/02_create_Jenkins_credential_for_ssh_in_multibranch_pipeline_job_scope_.png)

Step 3: Ssh into server that is running Jenkins as a docker container 

    ssh root@138.68.167.82

[Ssh into server hosting Jenkins docker container](/images/03_ssh_into_server_that_is_running_Jenkins_as_a_docker_container.png)

Step 4: Go into the Jenkins docker container as root user 

    docker exec -it u 0 dfb5b5a791bd bash 

[logging in Jenkins container as root](/images/04_go_into_the_jenkins_docker_container_as_a_root_user.png)


Step 5: Install terraform in Jenkins container as a root user 

[Intall Terraform in Jenkins container](/images/05_install_terraform_in_Jenkins_container_as_a_root_user.png)


Step 6: Verify if terraform command is available inside Jenkins container 

    terraform -v 

[Verifying if terraform is available](/images/06_verify_if_terraform_command_is_available_inside_Jenkins_container.png)

Step 7: Add terraform configuration files inside the application code, this means in the project add a new directory called terraform 

    mkdir terraform 

[Terraform folder](/images/07_add_terraform_configuration_files_inside_the_application_code_so_in_the_project_add_a_new_directory_called_terraform.png)


Step 8: Add main file into the terraform directory 

    touch terraform/main.tf 

[Add main file](/images/08_add_main_file_for_into_the_terraform_directory.png)

Step 9: In the main file put terraform configuration for the creation of the infastructure on aws, make sure that on the instance resource for key name put the key name of the key pair that was created earlier. This will enable Jenkins to ssh into ec2 instance 

note: I have a separate project that will be linked below for the creation of the infastructure and all configuration needed in the main file 

https://gitlab.com/omacodes98/terraform-automate-aws-infrastructure.git

[Main file config and changing key pair](/images/09_in_the_main_file_put_terraform_configuration_for_the_creation_of_the_infastructure_on_aws_make_sure_that_on_the_instance_resource_for_key_name_put_in_the_key_name_of_the_key_pair_that_was_created_earlier_this_will_enable_jenkins_to_ssh_into_ec2_ins.png)

Step 10: Include entry script in application code 

note: This entry script is one i created for the infastructure project link in above step 

[Entry script](/images/20_including_entry_script_in_application_code.png)

Step 11: Edit entry script to enable docker compose to be available on ec2 instance 

[Install Docker compose in script](/images/21_edit_entry_script_to_enable_docker_compose_to_be_available_on_ec2_instance.png)

Step 12: In the main file give variables default values this is because tfvars is included in gitignore file which means its not going to be available for jenkins, so having default values will mean we dont need to give values and also hard code the value 

[Variables and default values](/images/22_in_main_file_for_the_variables_give_them_default_values_this_is_because_tfvars_is_in_the_gitignore_file_which_means_its_not_going_to_be_available_for_jenkins_so_havuing_default_values_will_mean_we_dont_need_to_give_value_and_also_hard_code_the_valu.png)

Step 13: Move variables in main file to variables file which is good practice in terraform 

[Variables file](/images/23_move_the_variables_in_main_file_to_variablestf_file_which_is_good_practice_in_terraform.png)

Step 14: In Jenkinsfile add new stage provisioning right above deploy stage to automate the creation of the aws infastructure 

[Provisioning stage](/image/24_in_Jenkinsfile_add_new_stage_provisioning_right_above_deploy_stage_to_automate_the_creation_of_the_aws_infastructure.png)

Step 15: In Provisioning server stage set AWS access key id and secret acces key as environmental variables so terraform can authenticate 

[Environmental vars access key & secret access key](/images/25_in_provision_server_stage_set_AWS_access_key_id_and_secret_access_key_id_as_environmental_variable_so_terraform_and_jenkins_can_authenticate.png)

Step 16: Go into terraform directory to execute to creation of the infastructure by using dir(terraform) in Jenkinsfile provisioning server block 

[Execute creation of infastructure Jenkinsfile](/images/26_go_into_terraform_directory_to_execute_the_creation_of_the_infastructure.png)

Step 17: Swt terraform environmental variables to overwrite default value of environment prefix

[Terraform env variable in Jenkinsfile](/images/27_set_terraform_env_variable_in_Jenkinsfile_to_overwrite_default_value_of_env_prefix.png)

Step 18: In provision server stage add ec2 public ip address as an environment variable

[Set ec2 public ip as env var](/images/28_in_provision_server_stage_add_ec2_public_ip_address_as_an_environment_variable.png)

Step 19: Reference ec2 public ip address in deployment stage, this is to enable it to be dynamic. This will enable jenkins to ssh into ec2 instance and copy script and docker-compose file  through secure copy to ec2 instance

[ec2 public ip ad](/images/29_reference_ec2_public_ip_address_in_deployment_stage.png)

Step 20: Make sure the ssh key correct, change it to the ssh key that was created earlier 

note: The Jenkinsfile i am editing is a project that i worked on which is a complete CI CD pipeline that deploys an application on an ec2 instance using docker compose. The linke is below 

https://gitlab.com/omacodes98/cd-with-jenkins-to-ec2-instances-with-docker-compose.git


[Right ssh key](/images/30_make_sure_the_ssh_key_is_correct_change_it_to_the_ssh_key_that_was_created_earlier.png)

Step 21: Run pipeline and check for errors 

[Error](/images/31_check_for_errors.png)

Step 22: Add Jenkins ip address in variables file

[Jenkins ip in variable file](/images/32_add_jenkins_ip_in_variables_file.png)

Step 23: Add Jenkins ip to security group configuration in main file 

[Jenkins ip in sg config](/images/add_jenkins_ip_to_security_group_config_in_main_file.png)

Step 24: Add docker login to script to allow ec2 instance to have access to private docker repo which application is stored

[Adding docker login in docker compose](/images/37_add_docker_login_to_script_to_enable_applications_to_run_through_docker_compose.png)

Step 25: Add environmental variables for deployment stage needed by the script to authenticate for docker login 

[Deployment stage](/images/38_add_and_define_environmental_variables_that_in_deployment_stage_for_docker_user_pass.png)

Step 26: Push changes to application remote repo 

    git status
    git add .
    git commit -m "add jenkins ip"
    git push 

[Push change](/images/34_push_to_application_repo.png)

Step 27: Build Pipeline 

[Successful Pipeline](/images/39_successful_pipeline.png)

[logs](/images/40_logs.png)

Step 28: ssh into ec2 and check if the script was successful  

[Application running](/images/41_applications_running.png)


## Installation 

    brew install terraform 

## Usage 

    Terraform apply 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/complete-ci-cd-with-terraform.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/complete-ci-cd-with-terraform

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.