variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable subnet_cidr_block {
    default = "10.0.10.0/24"
}
variable avail_zone {
    default = "eu-west-2b"
}
variable env_prefix {
    default = "dev"
}
variable ip_address {
    default = "209.35.80.212/32"
}

variable jenkins_ip {
    default = "138.68.167.82/32"
}
variable instance_type {
    default = "t2.micro"
}

variable region {
    default = "eu-west-2"
}